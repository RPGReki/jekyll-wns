function loadYouTube(e = null) {
  if (e == null) return;
  var i = e.target.getAttribute('data-videoid');
  d.getElementById(i).innerHTML = d.getElementById('yt-' + i).innerHTML
}

$('.youtube a:first-child').on('click', loadYouTube);
$('.youtube a:nth-child(2)').on('click', loadYouTube);
